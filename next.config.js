const withPlugins = require('next-compose-plugins');
const withLess = require('next-with-less');
/** @type {import('next').NextConfig} */
const plugins = [
  [
    withLess,
    {
      lessLoaderOptions: {},
    },
  ],
];

module.exports = withPlugins(plugins, {
  reactStrictMode: false,
  swcMinify: true,
  output: 'standalone',
  images: {
    unoptimized: true,
  },

});