import {ApiBaseService} from "./base.service";
export class AuthService extends ApiBaseService {
  authConfig: IAuthConfig;

  constructor() {
    super();
    if(typeof window !== "undefined") {
      this.authConfig = {
        headers: {
          Referer: window.location.hostname
        }
      }
    }
  }
  public async register(payload: IRegisterPayload) {
    return (await this.httpClient.post("/auth/register", payload, this.authConfig))?.data;
  }

  public async login(payload: ILoginPayload) {
    return (await this.httpClient.post("/auth/login", payload, this.authConfig))?.data;
  }

  public async loginWithSSO(payload: ILoginSSOPayload) {
    return (await this.httpClient.post("/auth/sso", payload, this.authConfig))?.data;
  }

  public async resendVerifyEmail(payload: IResendVerifyEmailPayload) {
    return (await this.httpClient.post("/auth/verify-account/new", payload, this.authConfig))?.data;
  }

  public async forgetPassword(payload: IForgetPasswordPayload) {
    return (await this.httpClient.post("/auth/forget_password", payload, this.authConfig))?.data;
  }


  public async logout(payload: ILogoutPayload) {
    return (await this.httpClient.post("/auth/logout", payload, this.authConfig))?.data;
  }
}

export const authService = new AuthService();
