import WebOutlinedIcon from '@mui/icons-material/WebOutlined';
import RouterIcon from '@mui/icons-material/Router';
import CloudIcon from '@mui/icons-material/Cloud';
import {ROLES} from "@/configs/auth.config";
import StorageIcon from '@mui/icons-material/Storage';
import DevicesIcon from '@mui/icons-material/Devices';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
export const defaultNavigation = [
  {
    subheader: 'dashboard',
    key: 'dashboard',
    label: 'Dashboard',
    icon: WebOutlinedIcon,
    children: [
      {
        key: 'containers',
        href: '/container',
        label: 'Containers',
        icon: StorageIcon
      },
      {
        key: 'Images',
        href: '/images',
        label: 'Images',
        icon: CloudIcon
      },
      {
        key: 'volumes',
        href: '/volumes',
        label: 'Volumes',
        icon: RouterIcon
      },
      {
        key: 'devEnvironments',
        href: '/dev-environments',
        label: 'Dev Environments',
        icon: DevicesIcon
      }
    ]
  },
  {
    subheader: 'extensions',
    key: 'extensions',
    label: 'Extensions',
    icon: WebOutlinedIcon,
    children: [
      {
        key: 'add-extensions',
        href: '/add-extensions',
        label: 'Add Extensions',
        icon: AddCircleOutlineIcon
      }
    ]
  },
];


export const appNavigation = {
  [ROLES.USER]: defaultNavigation,
  [ROLES.ADMIN]: defaultNavigation,
  [ROLES.SYSADMIN]: defaultNavigation,
  [ROLES.ROOTSYSADMIN]: defaultNavigation
}

export const getMainRole = (roles: ROLES[]) => {
  const hasRoleSysAdmin = roles.find((role) => role === ROLES.SYSADMIN);
  const hasRoleAdmin = roles.find((role) => role === ROLES.ADMIN);
  const hasRoleUser = roles.find((role) => role === ROLES.USER);
  const hasRoleRootSysAdmin = roles.find((role) => role === ROLES.ROOTSYSADMIN);
  return hasRoleSysAdmin || hasRoleAdmin || hasRoleUser || hasRoleRootSysAdmin;
}
export const getNavigationsByRole = (roles: ROLES[]) => {
  return appNavigation[getMainRole(roles)] || [];
}

