const axiosConfigs = {
  development: {
    baseURL: 'https://sabo-api.hermess.site/api/',
    // baseURL: 'http://192.168.50.93:3001/api/',
    timeout: 10000
  },
  production: {
    baseURL: 'https://sabo-api.hermess.site/api/',
    timeout: 10000
  }
}
const getAxiosConfig = () => {
  const nodeEnv: NodeJS.ProcessEnv['NODE_ENV'] = process.env.NODE_ENV;
  return axiosConfigs[nodeEnv];
}

export const axiosConfig = getAxiosConfig();