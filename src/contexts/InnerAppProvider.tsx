import React, {useEffect} from "react";
import {SidebarProvider} from "@/contexts/SidebarContext";
import {AuthProvider, useAuthContext} from "@/contexts/AuthContext";
import {useAppDispatch} from "@/hooks/redux.hook";

interface InnerAppProps {
  children: React.ReactNode;
}

const InnerAppProvider = (props: InnerAppProps) => {
  const {account} = useAuthContext()
  const {children} = props;
  const dispatch = useAppDispatch();
  useEffect(function onLoad() {

  }, [])

  return <AuthProvider>
    <SidebarProvider>
        {children}
    </SidebarProvider>
  </AuthProvider>
}

export default InnerAppProvider;