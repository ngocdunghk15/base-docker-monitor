type Role = ROLES.ADMIN | ROLES.SYSADMIN | ROLES.USER | ROLES.ROOTSYSADMIN;

interface IAccount {
  _id: string;
  email: string;
  username: string;
  displayName: string;
  isActivated: boolean;
  roles: Role[]
  avatar: string;
  createdAt: string;
  updatedAt: string;
}

interface IPublicAccountByIDPayload {
  id: string;
}

interface IChangePasswordPayload {
  currentPassword: string;
  newPassword: string;
  confirmPassword: string;
}

interface IVerifyAccountPayload {
  verifyToken: string;
}

interface IUploadAvatarPayload {
  avatar: Form;
}

interface IActiveAccountPayload {
  id: string;
}

interface IUpdateProfilePayload {
  displayName?: string;
  gender?: string;
  roles?: ROLES[]
}

interface IChangePassWordPayload {
  newPassword: string;
  confirmPassword: string;
  verifyToken: string;
}

interface ILoadAccountResults {
  data: IAccount[],
  paginationOptions: IApiPagination
}