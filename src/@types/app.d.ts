type IQueries = Record<string, string | number | boolean>;
type LoadingStatus = "idle" | "pending" | "success" | "fail";

interface IApiPagination {
  totalDocs?: number;
  offset?: number;
  limit?: number;
  totalPages?: number;
  page?: number;
  pagingCounter?: number;
  hasNextPage?: boolean;
  hasPrevPage?: boolean;
  prevPage?: null | number;
  nextPage?: null | number;
}


