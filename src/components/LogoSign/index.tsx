import {
  styled, Typography,
} from '@mui/material';
import Link from 'src/components/Link';
import React from "react";
import {useRouter} from "next/router";

const LogoWrapper = styled("div")(
  ({theme}) => `
        color: ${theme.palette.text.primary};
        display: flex;
        text-decoration: none;
        font-weight: ${theme.typography.fontWeightBold};
        cursor: pointer;
        width: 100%;
        align-items: center;
`
);

function Logo() {
  const router = useRouter();
  return (
    <LogoWrapper onClick={()=>{
      router.push("/app/service-price");
    }}>
      <img style={{width: "56px", display: 'block'}} src="https://www.docker.com/wp-content/uploads/2022/03/Moby-logo.png"
           alt="Logo"/>
      <Typography sx={{width: 'fit-content', marginLeft: "12px", fontWeight: 700, fontSize:18}}>Docker Monitor</Typography>
    </LogoWrapper>
  );
}

export default Logo;
