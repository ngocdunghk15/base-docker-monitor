import {useRef, useState} from 'react';
import {
  Avatar,
  Box,
  Button,
  Divider,
  Hidden,
  lighten,
  List,
  ListItem,
  ListItemText,
  Popover,
  Typography
} from '@mui/material';
import {styled} from '@mui/material/styles';
import ExpandMoreTwoToneIcon from '@mui/icons-material/ExpandMoreTwoTone';
import AccountBoxTwoToneIcon from '@mui/icons-material/AccountBoxTwoTone';
import LockOpenTwoToneIcon from '@mui/icons-material/LockOpenTwoTone';
import {useAuthContext} from "@/contexts/AuthContext";
import {useAppDispatch} from "@/hooks/redux.hook";
import {resetSelectedReducersState} from "@/redux/actions/reset.action";
import {authService} from "@/services/auth.service";
import LocalStorageService from "@/services/storage.service";
import {AUTH_KEYS} from "@/configs/keys.config";
import WorkHistoryRoundedIcon from '@mui/icons-material/WorkHistoryRounded';
import {useRouter} from "next/router";

const UserBoxButton = styled(Button)(
  ({theme}) => `
        padding-left: ${theme.spacing(1)};
        padding-right: ${theme.spacing(1)};
`
);

const MenuUserBox = styled(Box)(
  ({theme}) => `
        background: ${theme.colors.alpha.black[5]};
        padding: ${theme.spacing(2)};
`
);

const UserBoxText = styled(Box)(
  ({theme}) => `
        text-align: left;
        padding-left: ${theme.spacing(1)};
`
);

const UserBoxLabel = styled(Typography)(
  ({theme}) => `
        font-weight: ${theme.typography.fontWeightBold};
        color: ${theme.palette.secondary.main};
        display: block;
`
);

const UserBoxDescription = styled(Typography)(
  ({theme}) => `
        color: ${lighten(theme.palette.secondary.main, 0.5)}
`
);

function HeaderUserbox() {
  const {account, setIsSignedIn} = useAuthContext()
  const dispatch = useAppDispatch()
  const {resetAccount} = useAuthContext();
  const ref = useRef<any>(null);
  const [isOpen, setOpen] = useState<boolean>(false);
  const router = useRouter()

  const handleOpen = (): void => {
    setOpen(true);
  };

  const handleClose = (): void => {
    setOpen(false);
  };

  const handleSignOut = () => {
    const {refresh_token}: IAuthToken = LocalStorageService.get(AUTH_KEYS.TOKEN, "");
    authService.logout({refreshToken: refresh_token}).then(async () => {
      await window.location.replace("/auth/sign-in");
      await new Promise(_ => setTimeout(_, 3000))
      setIsSignedIn(false)
      resetAccount()
      LocalStorageService.clear();
      dispatch(resetSelectedReducersState({}));
    });
  }
  return (
    <>
      <UserBoxButton color="secondary" ref={ref} onClick={handleOpen}>
        <Avatar variant="rounded" alt={account.data.displayName} src={account.data.avatar}/>
        <Hidden mdDown>
          <UserBoxText>
            <UserBoxLabel variant="body1">{account.data.displayName}</UserBoxLabel>
            <UserBoxDescription variant="body2">
              {account.data.email}
            </UserBoxDescription>
          </UserBoxText>
        </Hidden>
        <Hidden smDown>
          <ExpandMoreTwoToneIcon sx={{ml: 1}}/>
        </Hidden>
      </UserBoxButton>
      <Popover
        anchorEl={ref.current}
        onClose={handleClose}
        open={isOpen}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
      >
        <MenuUserBox sx={{minWidth: 210}} display="flex">
          <Avatar variant="rounded" alt={account.data.displayName} src={account.data.avatar}/>
          <UserBoxText>
            <UserBoxLabel variant="body1">{account.data.displayName}</UserBoxLabel>
            <UserBoxDescription variant="body2">
              {account.data.email}
            </UserBoxDescription>
          </UserBoxText>
        </MenuUserBox>
        <Divider sx={{mb: 0}}/>
        <List sx={{padding: "0"}}>
          <Button
            size={'small'}
            color="primary" fullWidth
onClick={handleSignOut}>
              <LockOpenTwoToneIcon />
              <ListItemText primary="Sign out"/>
          </Button>
        </List>
      </Popover>
    </>
  );
}

export default HeaderUserbox;
