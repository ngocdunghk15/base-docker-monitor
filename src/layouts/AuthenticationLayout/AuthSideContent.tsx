import {styled, Typography} from '@mui/material';
import {Box} from '@mui/system';
import {FC} from 'react';

const StyledProvider = styled(Box)(({theme}) => ({
  outline: 0,
  height: '100%',
  display: 'flex',
  overflow: 'hidden',
  position: 'relative',
  alignItems: 'center',
  flexDirection: 'column',
  justifyContent: 'center',
  backgroundColor: theme.palette.mode === "light" ? theme.palette.grey[300] : theme.palette.background.paper,
  '& .focusRing___1airF.carousel__slide-focus-ring': {
    outline: 'none !important',
  },
}));


const AuthSideContent: FC = () => {
  return (
    <StyledProvider
    >
      <SlideComponent
        img="/logo_banner.png"
      />
    </StyledProvider>
  );
};

export default AuthSideContent;

function SlideComponent({img}: { img?: string; }) {
  return (
    <Box style={{display:"flex",justifyContent:"center",height: "100vh", width: "100%", padding:0}}>
      <img src={img} height={"100%"} width={"auto"}/>
    </Box>
  );
}
